package observer.smsStation;

import java.util.Observable;

public class Message extends Observable implements Runnable{

    private int senderPhoneNumber;
    private String message;

    public Message(int senderPhoneNumber, String message) {
        this.senderPhoneNumber = senderPhoneNumber;
        this.message = message;
    }


    @Override
    public void run() {
        try{
            Thread.sleep(100*message.length());
            System.out.println("send sms");

            setChanged();
            notifyObservers(this);
        } catch (InterruptedException e){
            e.printStackTrace();
        }

    }



    public int getSenderPhoneNumber() {
        return senderPhoneNumber;
    }

    public void setSenderPhoneNumber(int senderPhoneNumber) {
        this.senderPhoneNumber = senderPhoneNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "new message from contact " + senderPhoneNumber + "\n" + message;
    }
}
