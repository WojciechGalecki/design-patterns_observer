package observer.smsStation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class SmsStation {

    private List<Phone> listNumbers = new ArrayList<>();
    private Executor anten = Executors.newFixedThreadPool(5);

    public SmsStation() {
        int number = 600600600;
        for (int i = 0; i < 10 ; i++) {
            Phone phone = new Phone(number++);
            listNumbers.add(phone);
        }
    }

    public void addPhoneNumber(int number){
        if(listNumbers.contains(number)) {
            System.out.println("Phone number already exists!");
        } else if(number/100000000 < 1){
            System.out.println("Required number of digits - 9!");
        } else {
            Phone p = new Phone(number);
            listNumbers.add(p);
        }
    }

    public void sendSms(String number, String content){
        try{
            int numberP = Integer.parseInt(number);
            Message sms = new Message(numberP,content);
            sms.addObserver(new Phone(numberP));
            anten.execute(sms);

        } catch (NumberFormatException e) {
            System.out.println("Wrong phone number format, required format: 000000000!!!");
        }
    }
}
