package observer.smsStation;

public class Main {
    public static void main(String[] args) {

        SmsStation smsStation = new SmsStation();
        smsStation.addPhoneNumber(600600600);
        smsStation.addPhoneNumber(60060060);
        smsStation.addPhoneNumber(600600800);
//        smsStation.sendSms("60060s600","bla bla bla");
        smsStation.sendSms("600600600","bla bla bla");
    }
}
