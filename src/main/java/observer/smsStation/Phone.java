package observer.smsStation;

import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {

    private int phoneNumber;

    public Phone(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public void update(Observable o, Object smsMessage) {
        if(smsMessage instanceof Message){
            Message sms = (Message) smsMessage;
            if(sms.getSenderPhoneNumber() == this.getPhoneNumber()){
                System.out.println("Number " + phoneNumber + " received " + smsMessage);
            }
        }
    }
}
