package observer.ChatRoom;

public class Message {
    private int senderID;
    private String message;

    public Message(int senderID, String message) {
        this.senderID = senderID;
        this.message = message;
    }

    public int getSenderID() {
        return senderID;
    }

    public void setSenderID(int senderID) {
        this.senderID = senderID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "New message from sender ID(" + senderID + ")" + "\n -> " + message;
    }
}
