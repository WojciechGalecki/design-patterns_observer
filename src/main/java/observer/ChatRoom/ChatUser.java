package observer.ChatRoom;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ChatUser implements Observer {
    private int id;
    private String nick;
    private List<String> messages = new ArrayList<>();
    private boolean isAdmin;

    public ChatUser(int id, String nick) {
        this.id = id;
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    @Override
    public void update(Observable o, Object receivedMessage) {
        if (receivedMessage instanceof Message) {
            Message msg = (Message) receivedMessage;
            if (msg.getSenderID() != this.getId()) {
                System.out.println(nick + " received " + receivedMessage);
                messages.add(msg.getMessage());
            }
        }

    }
}
