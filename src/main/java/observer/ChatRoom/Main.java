package observer.ChatRoom;

import observer.ChatRoom.ChatRoom;

public class Main {
    public static void main(String[] args) {

        ChatRoom room = new ChatRoom("Room");
        room.userLogin("admin");
        room.userLogin("Mateusz");
        room.userLogin("Agata");
        room.userLogin("Kleofas");

        room.sendMessage(2, "bla bla bla");
        room.kickUser(4, 1);

    }
}
