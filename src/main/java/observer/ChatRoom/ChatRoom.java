package observer.ChatRoom;

import com.sun.istack.internal.NotNull;

import java.util.*;

public class ChatRoom extends Observable {
    private static final List<String> adminList = new ArrayList<String>();
    private Map<Integer, ChatUser> userMap = new HashMap<>();
    private String roomName;
    private Integer userCounter = 0;

    public ChatRoom(String roomName) {
        this.roomName = roomName;

        this.adminList.add("admin");
        this.adminList.add("administrator");
    }


    public void userLogin(@NotNull String nick) {
        for (ChatUser user : userMap.values()) {
            if (user.getNick().equals(nick)) {
                System.out.println(nick + " User with this nick already exists.");
                return;
            }
        }
        ChatUser newUser = new ChatUser(++userCounter, nick);
        if (adminList.contains(newUser.getNick())) {
            newUser.setAdmin(true);
        }
        userMap.put(newUser.getId(), newUser);
        addObserver(newUser);
    }

    public void sendMessage(int senderId, String message) {
        setChanged();
        notifyObservers(new Message(senderId, message));
    }

    public void kickUser(int id_kickowanego, int id_kikujacego) {
        if (userMap.get(id_kikujacego).getNick().equals("admin") ||
                userMap.get(id_kikujacego).getNick().equals("administrator")) {
            deleteObserver(userMap.get(id_kickowanego));
            System.out.println("User " + userMap.get(id_kickowanego).getNick() + " has been deleted!");
        }
    }
}
